print("Entrez le prix HT")
HT = input()                                     # on demande à l'utilisateur d'entrer une valeur                                 #on donne une valeur de '1400' pour 'HT'
print('le prix HT est de:', HT)               #on affiche la valeur

tauxTVA = 0.2                                 #le taux de TVA en France est de 20%
                                            
montantTVA = float(HT) * float(tauxTVA)       #calcul du montant TVA,'float' car le prix peut être une valeure décimale 
TTC = float(HT) + float(montantTVA)           #calcul du montant TTC

print('Le prix total TTC est de:', TTC)       #affichage du montant TTC
print('Le cout de la TVA est de' , montantTVA) #on affiche le cout de la TVA