import java.util.Scanner;

public class question4 {
    public static void main(String[] args) {
    
        
        System.out.println("\n écrire la marque de la voiture");
        try (Scanner sc = new Scanner(System.in)) {
            String Marque = sc.nextLine();

            System.out.println("\n écrire le modèle de la voiture");
            try (Scanner sc2 = new Scanner(System.in)) {
                String Modele = sc2.nextLine();

                System.out.println("\n écrire le prix HT de la voiture");
                try (Scanner myOBJ = new Scanner(System.in)) {
                    double prixHT = myOBJ.nextDouble();
                    double TVA=1.2;
                    double prixTTC = TVA*prixHT ;

                    System.out.printf("votre voiture " +Marque +"est de" +Modele  +prixTTC);
                }
            }
        }
    }


}

