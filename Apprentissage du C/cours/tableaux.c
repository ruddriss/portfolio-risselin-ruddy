#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int tableau[4], i = 0;

    tableau[0] = 10;
    tableau[1] = 23;
    tableau[2] = 505;
    tableau[3] = 8;

    for (i = 0 ; i < 4 ; i++) // utilisation de for pour eviter de mettre un printf pour chaque case donc on a mis pour les valeur 
    //comprise entre 0 et 4 donc les case 0 a 4
    
    {
        printf("%d\n", tableau[i]); 
    }

    int tableau[4] = {0}; // ici Toutes les cases du tableau seront initialisées à 0



    int tableau[4] = {10, 23}; // Valeurs insérées : 10, 23, 0, 0 quand on indique la taille et qu'on ne precise pas les autre valeur alors c'est 0 automatiquement



    int tableau[] = {12, 19, 18, 2}; // pas besoin de renseigner la taille du tableau ici car le compleur le fait seul quand on indique chaque valeur
    return 0;


    int tableau[4] = {0}; // Toutes les cases du tableau seront initialisées à 0
}